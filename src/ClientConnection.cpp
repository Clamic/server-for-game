#include "ClientConnection.h"

#include <iostream>
#include <memory>
#include <cstring>

ClientConnection::ClientConnection()
{
    //ctor
}

ClientConnection::ClientConnection(const TCPsocket &clientConnection)
{
    p_clientSocket = clientConnection;
}

ClientConnection::~ClientConnection()
{
    //dtor
}

void ClientConnection::send(const std::string &command)
{
    const char* charConversion = command.c_str();

    void* data = const_cast<char*>(charConversion);
    int dataSize = command.length();

    int bytesSent = SDLNet_TCP_Send(p_clientSocket, data, dataSize);

    if(bytesSent < dataSize)
    {
        std::cout << "SEND FAILED\n";
    }

}

std::string ClientConnection::checkForMessages()
{

    char buffer[p_bufferSize];
    std::string message = "";
    memset(buffer, 0, p_bufferSize);

    if(SDLNet_SocketReady(p_clientSocket) == 0)
    {
        return "";
    }

    int incomingByteCount = SDLNet_TCP_Recv(p_clientSocket, buffer, p_bufferSize);

    if(incomingByteCount > 0)
    {
        buffer[incomingByteCount] = '\0';
        message = buffer;

        ///put if statement for if too much data was send (bigger than buffer)
    }
    else if(incomingByteCount < 0)
    {
        std::cout << "Lost connection with a client, please destroy this instance\n";
        active = false;
    }
    else
    {
        std::cout << "No message found, client may be disconnected\n";
    }

    return message;
}


void ClientConnection::closeConnection()
{

    SDLNet_TCP_Close(p_clientSocket);

}
