#include "ConnectionManager.h"

#include <iostream>

using namespace std;

ConnectionManager::ConnectionManager()
{

}

ConnectionManager::ConnectionManager(int16_t port, std::string levelName):
    p_levelName(levelName)
{
    /// ip = "" since a server only listens
    setupTCPConnection("", port);
}

void ConnectionManager::setupTCPConnection(const std::string &ip, int16_t port)
{
    p_port = port;

    p_clientSockets = SDLNet_AllocSocketSet(5);

    int resolveHost = SDLNet_ResolveHost(&p_serverIP, nullptr, p_port);
    if(resolveHost == -1)
    {
        cout << "server failed to be resolved\n";
        return;
    }

    p_server = SDLNet_TCP_Open(&p_serverIP);

    if(p_server == nullptr)
    {
        cout << "the socket could not be opened\n";
        return;
    }

    cout << "[SERVER] Server ready and listening to port " << p_port << "\n";

}

void ConnectionManager::checkConnections()
{
    for(auto ptr = std::begin(p_clientConnections); ptr != p_clientConnections.end(); ptr++)
    {
       if((*ptr)->isConnectionStillActive() == false)
       {
           SDLNet_TCP_DelSocket(p_clientSockets, (*ptr)->getSocket());
           p_clientConnections.erase(ptr);
           cout << "connection deleted\n";
           ///not the best solution
           return;
       }
    }
}

bool ConnectionManager::checkForNewClients()
{
    bool connectionFound = false;

    //SDLNet_CheckSockets

    if(p_server == nullptr)
    {
        cout << "[SERVER] The servers socket is not active\n";
        return false;
    }

    connectionFound = (SDLNet_SocketReady(p_server) == 0);

    cout << connectionFound << "\n";


    return connectionFound;
}

void ConnectionManager::broadcastCommand(const std::string& command)
{
    for(auto ptr = std::begin(p_clientConnections); ptr != std::end(p_clientConnections); ptr++)
    {
        (*ptr)->send(command);
    }

}


void ConnectionManager::addNewClient()
{

    TCPsocket sock = SDLNet_TCP_Accept(p_server);

    if(sock == nullptr)
    {
        return;
    }

    ClientConnection* newClient = new ClientConnection(sock);

    p_clientConnections.emplace_back(std::move(newClient));

    SDLNet_TCP_AddSocket(p_clientSockets, sock);

    cout << "[SERVER] A new client just connected. Clients connected = " << p_clientConnections.size() << "\n";

}

void ConnectionManager::listenToClients()
{

    if(SDLNet_CheckSockets(p_clientSockets, 0) <= 0)
    {
        return;
    }

    for(unsigned int i = 0; i < p_clientConnections.size(); i++)
    {
        string message = p_clientConnections.at(i)->checkForMessages();

        if(message.substr(0, 3) == "GET")
        {
            if(message.substr(4, 8) == "Level")
            {
                printf("getting level\n");
                p_clientConnections.at(i)->send(p_levelName);
            }
        }
    }


}

ConnectionManager::~ConnectionManager()
{
    //dtor
}
