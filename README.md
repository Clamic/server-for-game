# Simple Server-Client Server

> This code was written as part of the CPD project
> for COMP434.

> This is a console application which acts as a simple server that
> handles TCP connections.

## Author

Jozef Milenkiewicz

## Install

To compile this program the SDL and SDL_net APIs are required

## Usage

Compile Flags
~~~
-SDL -SDL_net
~~~

> When run the console will prompt the user to enter a port
> to listen to (must be above 1024) as well as the level name
> which clients will ask for on successful connection.

Main overview
To be able to have the program allow users to enter data as well
as have the program listen to incoming/existing TCP connections,
threads were used to listen for user commands as well as connections.

Class: ConnectionManager overview
This class manages the connnections of all existing and incoming TCP
connections.

Class: ClientConection overview
This class handles a single TCP socket and has the functionality to
check for and send data through the socket.

## License

MIT © Jozef Milenkiewicz 2018