#ifndef CLIENTCONNECTION_H
#define CLIENTCONNECTION_H

#include <SDL_net.h>

#include <string>


class ClientConnection
{
    public:
        ClientConnection();
        ClientConnection(const TCPsocket &clientConnection);

        void send(const std::string &command);
        std::string checkForMessages();
        TCPsocket& getSocket(){ return p_clientSocket; }

        bool isConnectionStillActive(){ return active; }

        virtual ~ClientConnection();

        void closeConnection();

    private:

        const int p_bufferSize = 30;

        TCPsocket p_clientSocket;

        bool active = true;
};

#endif // CLIENTCONNECTION_H
