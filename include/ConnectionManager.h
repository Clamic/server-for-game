#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include "SDL_net.h"
#include <string>
#include <vector>
#include <memory>

#include "ClientConnection.h"

class ConnectionManager
{
    public:

        ConnectionManager();
        ConnectionManager(int16_t port, std::string levelName);
        virtual ~ConnectionManager();

        void setupTCPConnection(const std::string &ip, int16_t port);
        /**
        Return:
        1 - a new connection was found
        0 - No new connections were found
        **/
        bool checkForNewClients();
        ///checks if connected clients are still connected
        void checkConnections();
        void listenToClients();
        void addNewClient();

        void send(const std::string &command);

        void broadcastCommand(const std::string &command);


    private:

        int16_t p_port;

        TCPsocket p_server;
        IPaddress p_serverIP;

        std::string p_levelName;

        SDLNet_SocketSet p_clientSockets;

        std::vector<ClientConnection*>  p_clientConnections;

        std::vector<std::string> clientMessage;



};

#endif // CONNECTIONMANAGER_H
