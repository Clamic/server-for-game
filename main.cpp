#include <iostream>

#include <SDL_net.h>
#include <SDL.h>

#include <string>
#include <thread>

#include "ConnectionManager.h"

using namespace std;

int16_t getPortFromUser();
string getLevelNameFromUser();
void init();
void commandsLoop();
void connectionLoop();
void quit();

    ///globals
    bool hasUserQuit;
    ConnectionManager serverManager;

int main(int argc, char* argv[])
{

    hasUserQuit = false;

    cout << "[SERVER] To begin, please enter a port\n";

    int16_t port = getPortFromUser();

    cout << "[SERVER] Now enter the level clients will load\n";

    string levelName = getLevelNameFromUser();

    init();

    serverManager = ConnectionManager(port, levelName);

    ///begin 2 while loops simultaneously, 1 waiting for commands and the other
    ///periodically checking the TCP socket
    thread commandThread(commandsLoop);
    thread connectionThread(connectionLoop);

    commandThread.join();
    connectionThread.join();

    quit();
    return 0;
}

int16_t getPortFromUser()
{

    int16_t port;

    string port_char = "1234";
    cin >> port_char;

    port = stoi(port_char);

    return port;
}

string getLevelNameFromUser()
{
    string levelName;

    cin >> levelName;

    return levelName;
}

void init()
{
    SDLNet_Init();
}

void commandsLoop()
{
    string command = "";
    char inputLine[50];

    while(!hasUserQuit)
    {
        cin.getline(inputLine, sizeof(inputLine));

        command = inputLine;

        if(command == "quit")
        {
            hasUserQuit = true;
        }
        if(command.substr(0, 5) == "SPAWN")
        {
            serverManager.broadcastCommand(command);
        }

    }


}


void connectionLoop()
{
    while(!hasUserQuit)
    {

        serverManager.checkConnections();
        serverManager.addNewClient();
        serverManager.listenToClients();

        SDL_Delay(1000);
    }


}

void quit()
{
    SDL_Quit();
    SDLNet_Quit();
}
